package com.gmail.khanhit100896.myapplication.Controller;

import com.gmail.khanhit100896.myapplication.Model.ResultUserLogin;

public class LoginController {

    private ResultUserLogin userLogin;

    public LoginController() {
        userLogin = new ResultUserLogin();
    }

    public ResultUserLogin setOnClick(String db, String username, String password){
        ResultUserLogin resultUserLogin;
        if(!username.equals("") && !password.equals("")){
            resultUserLogin = userLogin.doLogin(db,username,password);
            return resultUserLogin;
        }else{
            return null;
        }
    }
}
