package com.gmail.khanhit100896.myapplication.Model;

import com.gmail.khanhit100896.myapplication.Service.XPSApiEndpoint;
import com.gmail.khanhit100896.myapplication.Service.XPSClient;
import com.gmail.khanhit100896.myapplication.Service.XPSCredsParam;
import com.google.gson.Gson;

import org.json.JSONObject;

import retrofit2.Call;

public class ResultUserLogin extends User {

        private String uid;
        private String company_id;
        private String partner_id;
        private String db;
        private String session_id;
        private UserContext user_context;

        public ResultUserLogin() {
            super();
        }

        public ResultUserLogin(String username, String uid, String company_id, String partner_id, String db, String session_id, UserContext user_context) {
            super(username);
            this.uid = uid;
            this.company_id = company_id;
            this.partner_id = partner_id;
            this.db = db;
            this.session_id = session_id;
            this.user_context = user_context;
        }

        public String getUid() {
            return uid;
        }

        public String getCompany_id() {
            return company_id;
        }

        public String getDb() {
            return db;
        }

        public String getSession_id() {
            return session_id;
        }

        public UserContext getUser_context() {
            return user_context;
        }

        public class UserContext{
            private String lang;
            private String tz;
            private String uid;

            public UserContext(String lang, String tz, String uid) {
                this.lang = lang;
                this.tz = tz;
                this.uid = uid;
            }

            public String getLang() {
                return lang;
            }

            public String getTz() {
                return tz;
            }

            public String getUid() {
                return uid;
            }
        }

    public ResultUserLogin doLogin(String db, String username , String password) {
            ResultUserLogin resultUserLogin;
        XPSApiEndpoint xps = XPSClient.getXPSClient();
        try {
            Call<String> call = xps.authenticate(new XPSCredsParam(db, username, password));
            JSONObject objectResponse = new JSONObject(call.execute().body());

            if(objectResponse.has("result")) {

                Gson gson = new Gson();
                ResultUserLogin user = gson.fromJson(objectResponse.getString("result"), ResultUserLogin.class);
                XPSClient xpsClient = new XPSClient();
                xpsClient.session_id=user.getSession_id();

                resultUserLogin = new ResultUserLogin(user.getUsername(),user.uid,user.company_id,user.partner_id,user.db,user.session_id,
                        new UserContext(user.user_context.lang,user.user_context.tz,user.user_context.uid));

                return  resultUserLogin;
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }
}


