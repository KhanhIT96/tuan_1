package com.gmail.khanhit100896.myapplication.Model;

public class User {

    private String username;

    public User(String username) {
        this.setUsername(username);
    }

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
