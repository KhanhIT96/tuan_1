package com.gmail.khanhit100896.myapplication.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gmail.khanhit100896.myapplication.R;

import java.util.Objects;

public class InfoActivity extends AppCompatActivity {

    private TextView txtUserName;
    private TextView txtUserID;
    private TextView txtCompanyID;
    private TextView txtDB;
    private TextView txtSessionID;
    private TextView txtLang;
    private TextView txtTZ;
    private TextView txtUID;
    private Button btnSignOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        init();
        setData();

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                finish();
            }
        });
    }

    private void init() {
        this.txtUserName    = findViewById(R.id.txtUserName);
        this.txtUserID      = findViewById(R.id.txtUserID);
        this.txtCompanyID   = findViewById(R.id.txtCompanyID);
        this.txtDB          = findViewById(R.id.txtDB);
        this.txtSessionID   = findViewById(R.id.txtSessionID);
        this.txtLang        = findViewById(R.id.txtLang);
        this.txtTZ          = findViewById(R.id.txtTZ);
        this.txtUID         = findViewById(R.id.txtUID);
        this.btnSignOut     = findViewById(R.id.btnSignOut);
    }

    private void setData(){
        Intent intent = getIntent();
        this.txtUserName.setText(Objects.requireNonNull(intent.getExtras()).getString("username"));
        this.txtUserID.setText(intent.getExtras().getString("userid"));
        this.txtCompanyID.setText(intent.getExtras().getString("companyid"));
        this.txtDB.setText(intent.getExtras().getString("db"));
        this.txtSessionID.setText(intent.getExtras().getString("sessionid"));
        this.txtLang.setText(intent.getExtras().getString("lang"));
        this.txtTZ.setText(intent.getExtras().getString("tz"));
        this.txtUID.setText(intent.getExtras().getString("uid"));
    }
}
