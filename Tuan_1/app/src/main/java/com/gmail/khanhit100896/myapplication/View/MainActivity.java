package com.gmail.khanhit100896.myapplication.View;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.gmail.khanhit100896.myapplication.Controller.LoginController;
import com.gmail.khanhit100896.myapplication.Model.ResultUserLogin;
import com.gmail.khanhit100896.myapplication.R;

public class MainActivity extends AppCompatActivity {

    EditText edtUserName, edtPassword;
    Button btnLogin;
    CheckBox chkRemember;
    TextView txtSignUp;
    private final String db = "hhd_e300_vn_test01";
    private SharedPreferences sharedPreferences;

    private LoginController loginController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("DataLoginWithEmail",MODE_PRIVATE);

        init();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View v) {
                saveEmailPassword(edtUserName.getText().toString(),edtPassword.getText().toString());
                new AsyncTask<Void, Void, ResultUserLogin>() {
                    @Override
                    protected ResultUserLogin doInBackground(Void... voids) {
                       return loginController.setOnClick(db,edtUserName.getText().toString(),edtPassword.getText().toString());
                    }
                    @Override
                    protected void onPostExecute(ResultUserLogin resultUserLogin) {
                        Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
                        intent.putExtra("username", resultUserLogin.getUsername());
                        intent.putExtra("userid",resultUserLogin.getUid());
                        intent.putExtra("companyid",resultUserLogin.getCompany_id());
                        intent.putExtra("db",resultUserLogin.getDb());
                        intent.putExtra("sessionid",resultUserLogin.getSession_id());
                        intent.putExtra("lang",resultUserLogin.getUser_context().getLang());
                        intent.putExtra("tz",resultUserLogin.getUser_context().getTz());
                        intent.putExtra("uid",resultUserLogin.getUser_context().getUid());
                        startActivity(intent);
                    }
                }.execute();

            }
        });
    }

    private void init() {
        edtUserName     = findViewById(R.id.edtUserName);
        edtPassword     = findViewById(R.id.edtpassword);
        btnLogin        =findViewById(R.id.btnLogin);
        chkRemember     = findViewById(R.id.chkRemember);
        txtSignUp       = findViewById(R.id.txtSignUp);
        loginController = new LoginController();
        txtSignUp.setPaintFlags(txtSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        edtUserName.setText(sharedPreferences.getString("email",""));
        edtPassword.setText(sharedPreferences.getString("password",""));
        chkRemember.setChecked(sharedPreferences.getBoolean("checked",false));
    }

    private void saveEmailPassword(String email, String password){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(chkRemember.isChecked()){
            editor.putString("email",email);
            editor.putString("password",password);
            editor.putBoolean("checked",true);
            editor.apply();
        }
        else{
            editor.remove("email");
            editor.remove("password");
            editor.remove("checked");
            editor.commit();
        }
    }
}
